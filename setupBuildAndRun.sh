#!/bin/sh

MICROSERVICE=emptiness

echo "Your Payara is installed in ${PAYARA_HOME}"
echo "Building and deploying ${MICROSERVICE}"

cd ${MICROSERVICE} && mvn clean package -U
$PAYARA_HOME/bin/asadmin start-domain

$PAYARA_HOME/bin/asadmin deploy --force target/${MICROSERVICE}.war
cd ../${MICROSERVICE}-st && mvn clean package -U
mvn failsafe:integration-test failsafe:verify
open http://localhost:8080/${MICROSERVICE}/