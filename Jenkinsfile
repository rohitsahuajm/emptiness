def applicationName = env.CA_SERVICE;
def applicationNameST = "${applicationName}-st";
def releaseBuild = env.RELEASE_BUILD;
def throughputThreshold = env.THROUGHPUT_THRESHOLD;
def sonarProjectKey = env.SONAR_PROJECT_KEY;

pipeline{
    agent {
        label 'maven'
    }

    stages{
            stage('build') {
                steps{
                    echo "Building ${applicationName}"
                    sh script: "cd ${applicationName} && mvn -s src/main/config/maven/settings.xml -DskipTests clean package"   
                }
            }
            stage('build system tests') {
                steps{
                    sh script: "cd ${applicationNameST} && mvn -s ../${applicationName}/src/main/config/maven/settings.xml clean package"   
                }
            }    
            stage('qa with sonar') {
                steps{
                    sh script: "echo \'Provide sonar credentials and setup the sonarProjectKey, then remove comment\'"                    
//                    script {
//                            withCredentials([string(credentialsId: 'sonar', variable: 'TOKEN')]){
//                                sh "cd ${applicationName} && mvn -s src/main/config/maven/settings.xml -Dsonar.projectKey=${sonarProjectKey} -Dsonar.login=$TOKEN org.sonarsource.scanner.maven:sonar-maven-plugin:3.6.0.1398:sonar -Dsonar.host.url=https://itgov-ci.bmwgroup.net/sonar/ -Dsonar.java.binaries=target -Dsonar.java.sources=src/main/java -Dsonar.sources=src/main -Dsonar.exclusions=**/target/**,**/node_modules/**,**/build/**,**/bower_components/**,**/webapp/** -Dsonar.java.tests=src/test/java -Dsonar.jacoco.reportPaths=target/coverage-reports/jacoco-unit.exec"
//                            }   
//                        }
                    }
            }
            stage('unit tests') {
                steps{
                    sh script: "cd ${applicationName} && mvn -s src/main/config/maven/settings.xml test"   
                }
            }    
            stage('integration tests') {
                steps{
                    sh script: "cd ${applicationName} && mvn -s src/main/config/maven/settings.xml failsafe:integration-test failsafe:verify"   
                }
            } 
            stage('sti build'){
                steps{
                    script{
                        openshift.withCluster(){
                            openshift.withProject(){
                                def build = openshift.selector("bc", applicationName);
                                def startedBuild = build.startBuild("--from-file=\"./${applicationName}/target/${applicationName}.war\"");
                                startedBuild.logs('-f');
                                echo "${applicationName} build status: ${startedBuild.object().status}";                            
                            }
                        }
                    }
                }
            }
            stage('rollout'){
                steps{
                    script{
                        openshift.withCluster() {
                            openshift.withProject() {
                                def dc = openshift.selector('dc',applicationName)
                                dc.rollout().status()
                            }
                        }                
                    }
                }
            }
            stage('service verification'){
                steps{
                    openshiftVerifyService(svcName: applicationName);
                }
            }
            stage('system tests') {
                steps{
                    sh script: "cd ${applicationNameST} && mvn -s ../${applicationName}/src/main/config/maven/settings.xml failsafe:integration-test failsafe:verify"   
                }
            }    
            stage('torture tests') {
                steps{
                    sh script: "cd ${applicationNameST} && java -jar ./target/tt.jar"   
                }
            }    

            stage('verify TT results'){
                steps{
                    sh script: "cd ${applicationNameST} && mvn -DVERIFY_RESULTS=true -DTHROUGHPUT_THRESHOLD=${throughputThreshold} -s ../${applicationName}/src/main/config/maven/settings.xml failsafe:integration-test failsafe:verify"   
                }            
            }
            stage('tag image for promotion'){
                steps{
                    script{
                        def pom = readMavenPom file: 'pom.xml'
                        projectVersion = pom.version;
                        if(releaseBuild != null && (releaseBuild == true || releaseBuild == "true")){
                            echo "shouldRelease = ${releaseBuild} => tagging the docker image with ${projectVersion}"
                            openshiftTag(srcStream:applicationName,srcTag:"latest",destStream:applicationName,destTag: projectVersion);            
                        }else{
                            echo "shouldRelease = ${releaseBuild} => skipping tagging"
                        }
                    }
                }
            }        
        }               
}