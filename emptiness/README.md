# Purpose

A blank starting point for CA.micro projects. This template only contains beans.xml, preconfigured pom.xml with
Java EE 7 API, MicroProfile dependencies and a "Ping" JAX-RS endpoint.
