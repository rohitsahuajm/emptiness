#!/bin/sh
PROJECT_NAME=${OPENSHIFT_PROJECT_NAME:-ca-micro}

# The minimum amount of resources required to start Jenkins: https://docs.openshift.com/container-platform/3.6/dev_guide/compute_resources.html#dev-cpu-requests

CPU_REQUEST=1000m 
#default setting 256Mi
MEMORY_REQUEST=1G

CPU_LIMITS=4000m 
#default setting 256Mi
MEMORY_LIMITS=3G

echo '!!!!!!!!!!!!!!!'
echo '!!! WARNING !!!'
echo "To accelarate builds, this template is going to request a Jenkins with CPU: ${CPU_REQUEST} and RAM of: ${MEMORY_REQUEST}"
echo "The limit is set CPU: ${CPU_LIMITS} and RAM: ${MEMORY_LIMITS}"
echo '!!!!!!!!!!!!!!!'
echo '!!!!!!!!!!!!!!!'

CPU_REQUEST=2000m 
#default setting 256Mi
MEMORY_REQUEST=1G

CPU_LIMITS=4000m 
#default setting 256Mi
MEMORY_LIMITS=2G

APPLICATION_NAME=jenkins-ephemeral
oc new-app jenkins-ephemeral -n ${PROJECT_NAME}

DC_NAME=jenkins

oc patch dc/${DC_NAME} -p "{\"spec\":{\"template\":{\"spec\":{\"containers\":[{\"name\":\"${DC_NAME}\",\"resources\":{\"limits\":{\"cpu\":\"${CPU_LIMITS}\",\"memory\":\"${MEMORY_LIMITS}\"}}}]}}}}" -n ${PROJECT_NAME}
oc patch dc/${DC_NAME} -p "{\"spec\":{\"template\":{\"spec\":{\"containers\":[{\"name\":\"${DC_NAME}\",\"resources\":{\"requests\":{\"cpu\":\"${CPU_REQUEST}\",\"memory\":\"${MEMORY_REQUEST}\"}}}]}}}}" -n ${PROJECT_NAME}


