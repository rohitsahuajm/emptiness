#!/bin/sh
PROJECT_NAME=${OPENSHIFT_PROJECT_NAME:-ca-micro}
APPLICATION_NAME=${APPLICATION_NAME:-emptiness}

# CPU and Memory request increase for faster builds
CPU=2 
MEMORY=512Mi

# STI build setup
oc new-app bmw-payara5-sti:latest~/. --name=${APPLICATION_NAME}


# configuring build resources
oc patch bc/${APPLICATION_NAME} -p "{\"spec\":{\"resources\":{\"requests\":{\"cpu\":\"${CPU}\"}}}}"
oc patch bc/${APPLICATION_NAME} -p "{\"spec\":{\"resources\":{\"requests\":{\"memory\":\"${MEMORY}\"}}}}"
# configuring deployment resources
oc patch dc/${APPLICATION_NAME} -p "{\"spec\":{\"template\":{\"spec\":{\"containers\":[{\"name\":\"${APPLICATION_NAME}\",\"resources\":{\"requests\":{\"cpu\":\"${CPU}\",\"memory\":\"${MEMORY}\"}}}]}}}}"


# service exposed via route
oc expose svc ${APPLICATION_NAME} --port=8081
oc patch route/${APPLICATION_NAME} -p "{\"spec\":{\"tls\":{\"termination\":\"passthrough\"}}}" -n ${PROJECT_NAME}


# liveness and readiness probes setup
## "business" endpoint "changeit" is misused as probe. Refactor that accordingly
oc set probe dc ${APPLICATION_NAME} --liveness --initial-delay-seconds=100 --timeout-seconds=10 --get-url=https://:8081/${APPLICATION_NAME}/resources/changeit -n ${PROJECT_NAME}    
oc set probe dc ${APPLICATION_NAME} --readiness --initial-delay-seconds=100 --timeout-seconds=10 --get-url=https://:8081/${APPLICATION_NAME}/resources/changeit -n ${PROJECT_NAME}

# pipeline build setup
oc create -f pipeline.yaml -n ${PROJECT_NAME}