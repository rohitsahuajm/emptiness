#!/bin/sh
if [ -z "$1" ]
then
	echo "Invoke with $0 PATH_TO_SECRET"
	exit 1
fi
PATH_TO_SECRET=$1
PROJECT_NAME=${OPENSHIFT_PROJECT_NAME:-ca-micro}
oc secrets new-sshauth bitbucket --ssh-privatekey=${PATH_TO_SECRET} -n ${PROJECT_NAME}