//==============================================================================
// Copyright (c) 2019 BMW Group. All rights reserved.
//==============================================================================
package com.bmw.ca.emptiness.changeit.boundary;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import org.eclipse.microprofile.metrics.annotation.Metered;

/**
 *
 * Just an example endpoint used to generate Stress Test metrics for System
 * Tests. Delete it before shipping.
 */
@Path("changeit")
public class ChangeItResource {

    /**
     * The method generate metrics used in the build pipeline (stage: torture
     * tests) See
     * <a href="https://atc.bmwgroup.net/confluence/pages/viewpage.action?pageId=349935359">Stress
     * Tests / Torture Tests</a>
     *
     * @return sample string
     */
    @GET
    @Metered(absolute = true, name = "changeit", description = "tt metric")
    public String changeit() {
        /**
         * This method does not contain any business logic -> the unit test was
         * omitted on purpose.
         */
        return "Enjoy CA.micro! ...with or without clouds";
    }

}
