//======================================================================================================================
// Copyright (c) 2019 BMW Group. All rights reserved.
//======================================================================================================================
package com.bmw.ca.emptiness.changeit.boundary;

import com.bmw.ca.emptiness.DeploymentConfiguration;
import static com.bmw.ca.emptiness.DeploymentConfiguration.serverURI;
import static com.bmw.ca.emptiness.DeploymentConfiguration.tortureTestVerificationActive;
import com.bmw.ca.emptiness.TrustfulClientBuilder;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;
import org.junit.Before;
import org.junit.Test;

/**
 * This system test is only active after torture test / stress test execution.
 * The one minute rate throughput average is fetched from /metrics/application
 * and compared with the configured threshold.
 *
 * @author adam-bien.com
 */
public class ThroughputResultCheckIT {

    private Client client;
    private WebTarget tut;


    @Before
    public void init() {
        this.client = TrustfulClientBuilder.newClient();
        this.tut = serverURI(this.client).
                path("metrics").
                path("application");
    }


    @Test
    public void verifyPerformanceResults() {
        assumeTrue(tortureTestVerificationActive());
        JsonObject meteredResult = this.tut.request(MediaType.APPLICATION_JSON).
                get(JsonObject.class);
        assertFalse(meteredResult.isEmpty());
        JsonObject creationMetrics = meteredResult.getJsonObject("changeit");
        assertFalse(creationMetrics.isNull("oneMinRate"));
        double actualThreshold = creationMetrics.getJsonNumber("oneMinRate").
                doubleValue();
        double expectedThreshold = DeploymentConfiguration.throughputThreshold();

        boolean benchmarkPassed = actualThreshold >= expectedThreshold;
        String resultMessage = "failed";
        if (benchmarkPassed) {
            resultMessage = "passed";
        }

        System.out.println("-------------CA.micro-----------------");
        System.out.printf("%f ops / s reached. Benchmark %s. Threshold is: %f \n", actualThreshold, resultMessage, expectedThreshold);
        System.out.println("--------------------------------------");

        assertTrue(benchmarkPassed);
    }


}
