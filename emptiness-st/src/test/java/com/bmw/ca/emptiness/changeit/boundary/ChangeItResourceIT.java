//==============================================================================
// Copyright (c) 2019 BMW Group. All rights reserved.
//==============================================================================
package com.bmw.ca.emptiness.changeit.boundary;

import static com.bmw.ca.emptiness.DeploymentConfiguration.withBaseURI;
import com.bmw.ca.emptiness.TrustfulClientBuilder;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;

/**
 * System Tests are crucial for microservices
 */
public class ChangeItResourceIT {
    private Client client;
    private WebTarget tut;

    @Before
    public void init() {
        this.client = TrustfulClientBuilder.newClient();
        this.tut = withBaseURI(this.client, "/changeit/");
    }

    @Test
    public void health() {
        Response response = this.tut.
                request().
                get();
        assertThat(response.getStatus(), is(200));
        String message = response.readEntity(String.class);
        assertThat(message, containsString("CA.micro"));
    }

}
