//======================================================================================================================
// Copyright (c) 2019 BMW Group. All rights reserved.
//======================================================================================================================
package com.bmw.ca.emptiness.changeit.boundary;

import static com.bmw.ca.emptiness.DeploymentConfiguration.withBaseURI;
import com.bmw.ca.emptiness.TrustfulClientBuilder;
import java.util.concurrent.TimeUnit;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

@Warmup(iterations = 1, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 1, time = 1, timeUnit = TimeUnit.SECONDS)
@OutputTimeUnit(TimeUnit.SECONDS)
@BenchmarkMode(Mode.Throughput)
public class MessagesResourceBenchmark {

    @State(Scope.Benchmark)
    public static class JAXRSClient {

        WebTarget tut;
        Client client;

        @Setup(Level.Trial)
        public void initClient() {
            this.client = TrustfulClientBuilder.newClient();
            this.tut = withBaseURI(this.client, "/changeit/");
        }
    }

    @Benchmark
    public void ping(JAXRSClient testContext) {
        Response response = testContext.tut.
                request().
                get();
        assertThat(response.getStatus(), is(200));
        String message = response.readEntity(String.class);
        assertThat(message, containsString("CA.micro"));
    }

}
