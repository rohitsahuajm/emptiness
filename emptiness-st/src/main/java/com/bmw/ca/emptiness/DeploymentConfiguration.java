//==============================================================================
// Copyright (c) 2019 BMW Group. All rights reserved.
//==============================================================================
package com.bmw.ca.emptiness;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

/**
 * Fetches the URI from the Environment, System Properties, then uses a
 * fallback. Useful in Cloud Native (Docker / Kubernetes) deployments.
 *
 */
public interface DeploymentConfiguration {

    public static WebTarget withBaseURI(Client client, String path) {
        return serverURI(client).
                path("emptiness").
                path("resources").
                path(path);
    }

    public static WebTarget serverURI(Client client) {
        String baseUri = get("SERVICE_URI", "http://localhost:8080/");
        return client.target(baseUri);
    }


    public static String get(String key, String defaultValue) {
        String result = System.getenv().getOrDefault(key, System.getProperty(key, defaultValue));
        System.out.printf("Requested key: %s with defaultValue %s resolved to: %s \n", key, defaultValue, result);
        return result;
    }


    public static double throughputThreshold() {
        String threshold = get("THROUGHPUT_THRESHOLD", "5");
        return Double.parseDouble(threshold);
    }

    public static boolean tortureTestVerificationActive() {
        return !get("VERIFY_RESULTS", "unset").equals("unset");
    }

}
