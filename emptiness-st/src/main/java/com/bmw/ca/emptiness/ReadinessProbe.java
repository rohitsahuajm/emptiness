//======================================================================================================================
// Copyright (c) 2019 BMW Group. All rights reserved.
//======================================================================================================================
package com.bmw.ca.emptiness;

import static com.bmw.ca.emptiness.DeploymentConfiguration.withBaseURI;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

/**
 * In openshift environment the services could become earlier ready to handle
 * the traffic, than the load balancer / route. In such a case the LB will
 * return 503.
 *
 * This class retries until the first successful (200) request.
 *
 * @author airhacks.com
 */
public interface ReadinessProbe {

    long WAIT_BETWEEN_CHECKS = 2000;
    /**
     * the last URI segment. E.g. from
     * http://localhost:8080/emptiness/resources/changeit it is just /changeit/.
     */
    static final String READINESS_PATH = "changeit";


    /**
     * Blocks, until the GET request to READINESS_PATH returns 200 or total
     * number of 10 retries is reached
     *
     * @param client: the initialized client
     * @return the number of attempts
     */
    public static int waitUntilReady(Client client) {
        WebTarget probesTarget = withBaseURI(client, READINESS_PATH);
        int count = 0;
        int readinessStatus = -1;
        do {
            try {
                readinessStatus = probesTarget.request().get().getStatus();
            } catch (ProcessingException ex) {
                System.out.println("!!! " + ex.getCause());
                waitBeforeNextTry();
                continue;
            }
            if (readinessStatus == 200) {
                System.out.printf("attempt %d status %d connection successful \n", count, readinessStatus);
                return readinessStatus;
            }
            System.out.printf("attempt %d status %d \n", count, readinessStatus);
            waitBeforeNextTry();
        } while (count++ < 10);
        return readinessStatus;
    }

    static void waitBeforeNextTry() {
        try {
            Thread.sleep(WAIT_BETWEEN_CHECKS);
        } catch (InterruptedException ex) {
        }
    }
}
