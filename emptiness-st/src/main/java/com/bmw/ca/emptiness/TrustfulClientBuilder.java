//==============================================================================
// Copyright (c) 2019 BMW Group. All rights reserved.
//==============================================================================
package com.bmw.ca.emptiness;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

/**
 * The JAX-RS Client created by this class does not verify SSL certificates and
 * is so useful in openshift environments where new servers are dynamically
 * provisioned.
 *
 */
public class TrustfulClientBuilder {

    /**
     * @return the Client after successful execution of the readiness probe
     */
    public static Client newClient() {
        HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
        TrustManager[] noopTrustManager = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] xcs, String string) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] xcs, String string) {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            }
        };
        try {
            SSLContext sc = SSLContext.getInstance("ssl");
            sc.init(null, noopTrustManager, null);
            Client client = ClientBuilder.newBuilder().
                    sslContext(sc).
                    build();
            ReadinessProbe.waitUntilReady(client);
            return client;

        } catch (KeyManagementException | NoSuchAlgorithmException ex) {
            throw new IllegalStateException("JAX-RS (SSL) client initialization exception ", ex);
        }
    }


}
