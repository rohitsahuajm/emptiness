# 2019.3 release

## archetype / emptiness

location of the system test was fixed
sonar stage commented out
small fixes in openshift scripts
CPU request lowered to prevent long wait times in dev cluster

# 2019.02 release

## archetype / emptiness

ping renamed to changeit
superfluous keys removed from pipeline.yaml
APPLICATION_NAME in scripts exposed as a variable

# 2019.01 release

## simplicity

torture tests implemented
business metrics refactored and streamlined
pipeline extended with TT stages
throughput metrics exposed for verifications within pipeline
MessageOperationsCounter drastically simplified
System Tests are shipping with ReadinessProbe loop which waits until the LB becomes ready (for 503 error prevention)
The throughput established during TT is verified with a dedicated ST

## complexity

"unchecked" compiler warnings removed

## emptiness / generated with archetype

staging script added
TT (torture test / stress test) added
dedicated ST implemented for stress tests result evaluation
ReadinessProbe implemented for 503 detection / mitigation
throughput is configurable in the pipeline
pipeline extended with TT stages
THROUGHPUT_THRESHOLD is configurable as pipeline parameter
STI build waits until fully configured
STI build also configures the minimum amount of memory and CPU for DC
Route configuration changed: `/metrics`, `/health`, `/openapi` routes are available without further configuration changes
The order of stages in the execution pipeline was swapped. The System Tests together with Stress Tests are immediately executed after the
build step. Now the STI build is only started after successful compilation of ST.