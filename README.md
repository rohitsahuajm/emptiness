# purpose

A blank template for CA.micro projects. 

> clone, rename, deploy.

# local quickstart

## prereqs

1. Java 8 is properly configured
2. PAYARA_HOME environment entry points to your local Payara 4.182+ installation. 

## deployment && system test

1. Clone sources: https://[qnumber]@atc.bmwgroup.net/bitbucket/scm/camicro/emptiness.git
2. cd `emptiness`
3. `./setupBuildAndRun.sh`

The script `./setupBuildAndRun.sh` comprises:

```
cd emptiness && mvn clean package
$PAYARA_HOME/bin/asadmin start-domain

$PAYARA_HOME/bin/asadmin deploy --force target/emptiness.war
cd ../emptiness-st && mvn clean package
mvn failsafe:integration-test failsafe:verify
open http://localhost:8080/emptiness/
```

## bitbucket setup
 
1. Create an empty bitbucket repository at: [https://atc.bmwgroup.net/bitbucket/](https://atc.bmwgroup.net/bitbucket/)
2. Push `emptiness` folder (`emptiness` and `emptiness-st`) to the freshly created repository
3. Generate SSL keys [https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html)
4. Add the public key to your bitbucket repository: `Repository Settings > Access keys`

## openshift setup

The name of the openshift project defaults to `ca-micro`. The name can be configured by setting the environment entry: OPENSHIFT_PROJECT_NAME with e.g. `export OPENSHIFT_PROJECT_NAME=testwebapp`. All scripts are located in: `(...)/emptiness/emptiness/src/main/config/openshift/`.

0. Login to openshift: oc login https://mp-dev-cnap-master.bmwgroup.net --token=[YOUR_TOKEN]
1. Create secret with the corresponding private key and the conventional name "bitbucket": `oc secrets new-sshauth bitbucket --ssh-privatekey=[PATH_TO_SECRET] -n [YOUR_OPENSHIFT_PROJECT_NAME]`, or with the script: `./0_create_secret.sh`. Now your openshift build should be able to clone `emptiness`.
2. Create a "Source to Image" build with `oc new-app bmw-java-premium-sti (...)`. The script `1_create_STI_build.sh` performs the creation with some hardcoded, conventional parameters. The script will fail at the first execution with the error `Fetch source failed`. All subsequent executions should be successful.
3. Install Jenkins with: `oc new-app jenkins-ephemeral -n ca-micro` or use the provided script: `./2_create_jenkins.sh`. 
4. Create the build pipeline with: `oc create -f emptiness-pipeline.yaml -n ca-micro` or use the script: `./3_create_pipeline.sh`. The environment entries in `pipeline.yaml` have to be changed accordingly. `SERVICE_URI` should point to the exposed URI (default: `https://emptiness-ca-micro.mp-dev-cnap.bmwgroup.net/`), `REPLICA_COUNT` comes with 1 and can be changed to start more instances of your service and `CA_SERVICE` should be named after your renamed service (default: `emptiness`). The variables can be also passed as arguments at start e.g.: `oc start-build -e SERVICE_URI=https://emptiness-testwebapp.mp-dev-cnap.bmwgroup.net emptiness-pipeline`.
5. Set liveness: `oc set probe dc emptiness --liveness --get-url=https://:8081/emptiness/resources/ping -n ca-micro` and readiness probes: `oc set probe dc emptiness --readiness --get-url=https://:8081/emptiness/resources/ping -n ca-micro` or execute the script: `./4_set_probes.sh`. WARNING: `emptiness` does not ship with explicity liveness and readiness probes. The "business" endpoint `/resources/ping` is misused as both probes.
6. To delete `emptiness` again execute: `./100_delete_service.sh` or `oc delete all --selector ca.service=emptiness -n ca-micro`

## sonar (ATC CI) setup

The `Jenkinsfile` comes with integrated Sonar plugin which requires an API-Token. To generate the API token:

1. Order sonar [https://atc.bmwgroup.net/confluence/pages/viewpage.action?pageId=175413273](https://atc.bmwgroup.net/confluence/pages/viewpage.action?pageId=175413273)
2. Rename the `camicro` build parameter to your sonar project key.
2. Generate the API-Token and store the API token as Jenkins `secret text` (Jenkins -> Credentials -> System -> Globa credentials -> Add Credentials, Kind: secret text) credential with the id `sonar`. The pipeline will use a credential with the id `sonar` for Sonar authentication.

For additional Sonar CI information at BMW checkout: [https://atc.bmwgroup.net/confluence/pages/viewpage.action?pageId=175165695](https://atc.bmwgroup.net/confluence/pages/viewpage.action?pageId=175165695)